/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
const app = new Vue({
  el: "#app",
  data: { // Dep.id = 2
    message: "Vue.js 源码分析",
    person: { // Dep.id = 4-5
      name: "doubleWJ"
    },
    list: ['SOS', '234', 'Hello', 'Mac'], // Dep.id = 7-8
    zero: 'This is a Component',
    coder: `<div class="point">
                <h2>灵活</h2>
                <p>不断繁荣的生态系统，可以在一个库和一套完整框架之间自如伸缩。</p>
              </div>`
  },
  mounted() {

  },
  methods: {

  },
  render(h) {
    return h(ViewZoo);
  },
});

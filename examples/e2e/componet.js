const ViewZoo = Vue.component('ViewZoo', {
  props: {
    zero: {
      type: String,
      default: 'Coo'
    }
  },
  data() {
    return {
      coco: "巨大的CoCo，冲啊！",
      logger: '变化的CoCo，爆炸吧!'
    }
  },
  methods: {
    handler() {
      if (this.logger != this.coco) {
        const cpx = this.coco;
        this.coco = this.logger;
        this.logger = cpx;
      }
    }
  },
  render(h) {
    return h('div', {
      class: {
        app: true
      }
    }, [
      h('h3', {}, '剑来'),
      h('div', {}, this.zero),
      h('div', {
        class: {
          modify: true
        },
        on: {
          click: this.handler
        }
      }, '点击操作，触发组件Watcher，进行patchVnode比较，去吧，皮卡丘！！！'),
      h('div', {}, this.coco),
    ]);
  },
  watch: {
    coco(val) {
      // vm是Vue实例对象，可以通过自定义watch观察vm._watchers数量
      console.log(val);
    }
  },
});

const ViewZce = Vue.component('ViewZce', {
  name: 'ViewZce',
  template: `<h1>Vue.component</h1>`
});
